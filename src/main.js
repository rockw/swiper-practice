import { createApp } from 'vue'
import App from './App.vue'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'swiper/swiper-bundle.css'
import 'swiper/css/swiper.css'

createApp(App).user(VueAwesomeSwiper).mount('#app')
